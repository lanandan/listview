package com.example.listview;

import android.support.v7.app.ActionBarActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends ActionBarActivity {
String[] lst_string={"Google","Face Book","Twitter","Linked In","Yahoo","Whats app","Viber","IMO","Skype","Google Plus",
		"Tango","Shared In"};
ArrayAdapter<String> adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ListView li=(ListView)findViewById(R.id.lst_items);
		adapter=new ArrayAdapter<String>(this,R.layout.listview_single,R.id.txt_item,lst_string);
		li.setAdapter(adapter);
	}
	}
